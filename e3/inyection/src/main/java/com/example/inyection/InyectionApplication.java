package com.example.inyection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InyectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(InyectionApplication.class, args);
	}

}
