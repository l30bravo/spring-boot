package com.example.inyection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import javax.management.Notification;


//Contruccion inyeccion
@RestController
public class PageController {

    private NotificationService notificationService;

    @Autowired
    public PageController(NotificationService notificationService){
        this.notificationService = notificationService;
    }
}

//Example Setter
/*
@RestController
public class PageController {

    private NotificationService notificationService;

    @Autowired
    public void setNotificationService(NotificacionService notificacionService){
        this.notificationService = notificationService;
    }
}
*/


//Gestion de inyeccion
/*
@RestController
public class PageController {

    @Autowired
    private NotificationService notificationService;

    public PageController(){
        notificationService = new NotificationService();
    }

    @RequestMapping("/")
    public String home(){
        return "Hollow Page Controller!!";
    }
}
*/